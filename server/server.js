//import modules installed at the previous step. We need them to run Node.js server and send emails
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const nodemailer = require("nodemailer");
const details = require("./details.json");

// create a new Express application instance 
const app = express();
app.use(cors({ origin: "*"}));
app.use(bodyParser.json());

app.listen(3000, () => {
  console.log("The server started on port 3000");
});

app.get("/", (req, res) => {
    res.send(
        "<h1 style='text-align: center'>Email Host</h1>"
    );
});


app.post("/sendmail", (req, res) => {
  console.log("request came");
  let user = req.body;
  sendMail(user, info => {
    console.log(`The email has been sent 😃`);
    res.send(info);
  });
});

async function sendMail(user, callback){
    let transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 587,
      secure: false,
      auth: {
        user: details.email,
        pass: details.password
      }
    }); 

    let mailOptions = {
        from: '"CCA Website" <info@caprockapps.com>',
        to: details.email, 
        subject: "I am an inquiry from " + user.name + " (" + user.email + ")",
        html: user.message 
      };
      
    let info = await transporter.sendMail(mailOptions);
    callback(info);
}

  