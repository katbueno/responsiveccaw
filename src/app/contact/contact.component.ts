import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { HttpService } from '../services/http.service';


import {
  trigger,
  state,
  style,
  animate,
  transition,
  keyframes,
  group,
  query,
  sequence,
  stagger,
  animation,
  useAnimation,
  animateChild
} from '@angular/animations';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {

  isSent: boolean = false;
  resultMessage: String;
  contactForm = new FormGroup ({
    email: new FormControl('',[
      Validators.required,
      Validators.email
    ]),
    name: new FormControl(''),
    message: new FormControl(''),
  });



  constructor(private http: HttpService) {}

  ngOnInit() {
  }




  messageUs(){
    let user = {
      name: this.contactForm.get('name').value,
      email: this.contactForm.get('email').value,
      message: this.contactForm.get('message').value,
    }
   
    this.http.sendEmail("http://localhost:3000/sendmail", user).subscribe(
      data => {
        let res:any = data;
        console.log("Email Sent");
        this.resultMessage = "Your Message Has Been Sent!"
        this.isSent = true;
        
        
      },
      err => {
        console.log(err);
        this.contactForm.reset();
        this.isSent = true;
        this.resultMessage = 'Unknown Error Occured';
      },() => {
        console.log("");
      }
    );


    
   
  
  }
}
